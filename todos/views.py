from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todoitem = TodoList.objects.get(id=id)
    context = {
        "todoitem": todoitem,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.id)
    else:
        form = TodoListForm()
    context = {"form": form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todoitem = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todoitem)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.id)
    else:
        form = TodoListForm(instance=todoitem)

    context = {"form": form}
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todoitem = TodoList.objects.get(id=id)
    if request.method == "POST":
        todoitem.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.id)
    else:
        form = TodoItemForm()

    context = {"form": form}

    return render(request, "todos/item_create.html", context)


def todo_item_update(request, id):
    todoitem = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.id)
    else:
        form = TodoItemForm(instance=todoitem)

    context = {"form": form}
    return render(request, "todos/item_update.html", context)
